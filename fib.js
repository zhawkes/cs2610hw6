// Set the document title
document.title = 'TODO: Dynamic Fibonacci Sequence in JavaScript';

// Create a red div in the body
var div = document.createElement('div');
div.setAttribute('class', 'red shadowed stuff-box');
document.querySelector('body').appendChild(div);

// Add instructions
var para = document.createElement('p');
para.textContent = "Move the slider to see the Fibonacci sequence of the selected length";
div.appendChild(para);

//create the form
var form = document.createElement('form');
div.appendChild(form);
var input = document.createElement('input');
var label = document.createElement('label');
form.appendChild(label);
label.textContent = "Fib(0)";
form.appendChild(input);
input.setAttribute("type", "range");
input.setAttribute("id", "fibSlider");
input.setAttribute("max", "50");
input.setAttribute("min", "0");
input.setAttribute("value", "0");
//create div
const displayDiv = document.createElement("div");
displayDiv.setAttribute("class", "flex");
div.appendChild(displayDiv);


//onchange function
input.onchange = function display(){
  displayDiv.innerHTML = "";
  const value = input.value;
  label.textContent =  "fib(" + value + ")";
  let temp = 0;
  let fibNumbers = [];
  while(temp < value){
    fibNumbers.push(fibIterative(temp));
    temp++;
  }
  for(var i = 0; i < fibNumbers.length; i++){
    let element = document.createElement("div")
    element.textContent = "[" + fibNumbers[i] + "]";
    console.log(i);
    displayDiv.appendChild(element);
  }
}

function fibIterative(n){
  var a = 1;
  var b = 0;
  var temp;

  while (n >= 0){
    temp = a;
    a = a + b;
    b = temp;
    n--;
  }

  return b;
}

//Start extra credit for showing recursive calls
var recursiveDiv = document.createElement('div');
recursiveDiv.setAttribute('class', 'red shadowed stuff-box');
document.querySelector('body').appendChild(recursiveDiv);
var para2 = document.createElement('p');
para2.textContent = "Move the slider to see the recursive call tree for the fibonacci function";
recursiveDiv.appendChild(para2);
//create recursive form and slider
var recursiveForm = document.createElement('form');
recursiveDiv.appendChild(recursiveForm);
var recursiveLabel = document.createElement('label')
recursiveLabel.textContent = "fib(0)";
recursiveForm.appendChild(recursiveLabel);
var recursiveInput = document.createElement('input');
recursiveForm.appendChild(recursiveInput);
recursiveInput.setAttribute("type", "range");
recursiveInput.setAttribute("id", "fibRecursive");
recursiveInput.setAttribute("max", "11");
recursiveInput.setAttribute("min", "0");
recursiveInput.setAttribute("value", "0");
//create div for the recursive display
const recursiveDisplayDiv = document.createElement("div");
recursiveDisplayDiv.setAttribute("class", "fib");
recursiveDiv.appendChild(recursiveDisplayDiv);

recursiveInput.onchange = function fibRecursive(){
  recursiveDisplayDiv.innerHTML = "";
  const value = recursiveInput.value;
  recursiveLabel.textContent = "fib(" + value + ")";
  const parentDiv = document.createElement('div');
  parentDiv.setAttribute("class", "flex");
  recursiveDisplayDiv.appendChild(parentDiv);
  recursiveDisplayDiv.setAttribute("class", "flex");
    const fibValue = fibonacci(value);
    const valueDiv = document.createElement('div');
    valueDiv.textContent = "Fib(" + value + ")=" + fibValue.value;
    parentDiv.appendChild(valueDiv);
    parentDiv.appendChild(fibValue.html);
}

//has to return a node;
function fibonacci(n){
  if(n <= 1){
    const fib = document.createElement('div');
    fib.setAttribute("class", "fib");
    return {"value": n, "html": fib};
  }
  const x = n-1;
  const y = n-2;
  const leftValue = fibonacci(x);
  const rightValue = fibonacci(y);
  const left = document.createElement('div');
  const right = document.createElement('div');
  left.setAttribute("class", "fib-left");
  right.setAttribute("class", "fib-right");
  left.textContent = "fib(" + x + ")=" + leftValue.value;
  right.textContent = "fib(" + y + ")=" + rightValue.value;
  const ultimateDiv = document.createElement('div');
  ultimateDiv.setAttribute("class", "fib");
  ultimateDiv.appendChild(left);
  ultimateDiv.appendChild(right);
  return {"value": leftValue.value + rightValue.value, "html": ultimateDiv};
}
